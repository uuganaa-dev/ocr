import React, { useState } from "react";
import Scanner from "./Scanner";
import "./App.css";

const App = () => {
  const [result, setResult] = useState({});

  const handleScan = (data) => {
    setResult(data);
  };

  return (
    <div className="App">
      <h1>Barcode and Phone Number Scanner</h1>
      <Scanner onScan={handleScan} />
      {result.barcode && <p>Scanned Barcode: {result.barcode}</p>}
      {result.phoneNumber && <p>Detected Phone Number: {result.phoneNumber}</p>}
    </div>
  );
};

export default App;
