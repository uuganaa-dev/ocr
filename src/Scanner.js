import React, { useState } from "react";
import { BrowserMultiFormatReader } from "@zxing/library";
import Tesseract from "tesseract.js";

const Scanner = ({ onScan }) => {
  const [image, setImage] = useState(null);
  const [barcode, setBarcode] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");

  const handleImageChange = async (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onloadend = () => {
        setImage(reader.result);
        scanBarcode(reader.result);
        readText(reader.result);
      };
      reader.readAsDataURL(file);
    }
  };

  const scanBarcode = async (imageData) => {
    const codeReader = new BrowserMultiFormatReader();
    try {
      const result = await codeReader.decodeFromImage(undefined, imageData);
      setBarcode(result.text);
      onScan({ barcode: result.text });
    } catch (err) {
      console.error(err);
    }
  };

  const readText = async (imageData) => {
    Tesseract.recognize(imageData, "eng", {
      logger: (m) => console.log(m),
    }).then(({ data: { text } }) => {
      const phoneNumberMatch = text.match(/\b\d{8}\b/);
      if (phoneNumberMatch) {
        setPhoneNumber(phoneNumberMatch[0]);
        onScan({ phoneNumber: phoneNumberMatch[0] });
      }
    });
  };

  return (
    <div>
      <input type="file" accept="image/*" onChange={handleImageChange} />
      {image && <img src={image} alt="Scanned" width="300" />}
      <p>Scanned Barcode: {barcode}</p>
      <p>Detected Phone Number: {phoneNumber}</p>
    </div>
  );
};

export default Scanner;
